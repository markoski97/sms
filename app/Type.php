<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $table = 'user_types';

    public function profiles(){

       return $this->hasMany(Profile::class);
    }
}
