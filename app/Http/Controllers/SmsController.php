<?php

namespace App\Http\Controllers;

use App\Group;
use App\Profile;
use http\Client;
use Illuminate\Http\Request;

class SmsController extends Controller
{
    public function index(){
        $groups=Group::all();
        $profiles=Profile::all();

        $alls=$groups->merge($profiles);

        return view('send_sms',compact('alls'));
    }

    public function sendCustomMessage(Request $request)
    {
        $validate = $request->validate([
            'profiles' => 'required|array',
            'body' => 'required',
        ]);
        $profiles = $validate["profiles"];
        foreach ($profiles as $profile) {
            $this->sendMessage($validate["body"], $profile);
        }
        return back()->with(['success' => "Sms sent"]);
    }
    private function sendMessage($message, $recipients)
    {
        //sms provider
        $account_sid = getenv("acc of the sms provider api");
        $auth_token = getenv("auth token");
        $number = getenv("phone number to send from");

        $client = new Client($account_sid, $auth_token);
        $client->messages->create($recipients, array('from' => $number, 'body' => $message));
    }

}
