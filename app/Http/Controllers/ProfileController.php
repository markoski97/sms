<?php

namespace App\Http\Controllers;

use App\Profile;
use App\Type;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules\RequiredIf;

class ProfileController extends Controller
{

    public function index()
    {
        $profiles = Profile::all();
        return view('profile_index', compact('profiles'));
    }

    public function create()
    {
        $types = Type::all();

        return view('profile_create', compact('types'));
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'type' => 'required',
            'name' => 'required',
            'email' => 'required|email',
            'phone_number' => 'required|numeric',
            'tax_number' => new RequiredIf($request->type == 2, 'numeric')
        ]);

        $profile = new Profile();

        $profile->type_id = $request->type;
        $profile->name = $request->name;
        $profile->email = $request->email;
        $profile->phone_number = $request->phone_number;
        $profile->tax_number = $request->tax_number;

        $profile->save();
        return back()->with(['success' => "{$request->name} registered"]);
    }


    public function edit($id)
    {
        $profile = Profile::findOrFail($id);

        $types = Type::all();

        return view('profile_edit', compact('profile', 'types'));
    }


    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'type' => 'required',
            'name' => 'required',
            'email' => 'required|email',
            'phone_number' => 'required|numeric',
            'tax_number' => new RequiredIf($request->type == 2, 'numeric')
        ]);

        $profile = Profile::findOrFail($id);

        $profile->type_id = $request->type;
        $profile->name = $request->name;
        $profile->email = $request->email;
        $profile->phone_number = $request->phone_number;

        if ($request->type == 2) {
            $profile->tax_number = $request->tax_number;
        } else
            $profile->tax_number = null;
        $profile->save();
        return back()->with(['success' => "{$request->name} updated"]);
    }


    public function destroy($id)
    {
        $profile = Profile::findOrFail($id)->delete();

        return back()->with(['success' => "user deleted"]);
    }
}
