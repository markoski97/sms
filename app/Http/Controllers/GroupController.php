<?php

namespace App\Http\Controllers;

use App\Group;
use App\Profile;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    public function index(){

        $groups=Group::all();

        return view('group_index',compact('groups'));
    }

    public function create(){
        $profiles=Profile::all();
        return view('group_create',compact('profiles'));
    }

    public function store(Request $request){

        $group=new Group();

        $group->name=$request->name;

        $group->save();

        return back()->with(['success' => "{$request->name} registered"]);

    }

    public function AddUser(){
        $groups=Group::all();
        $profiles=Profile::all();
        return view('add_users_to_group',compact('profiles','groups'));
    }

    public function StoreAddUser(Request $request){

        $group=Group::findorfail($request->group);
        $group->save();
        $group->profiles()->attach($request->profiles);
        return back()->with(['success' => "{$request->name} registered"]);
    }
}
