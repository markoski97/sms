<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    public function type(){

       return $this->belongsTo(Type::class);
    }

    public function group()
    {
        return $this->belongsTo('App\Group');
    }

    public function groups(){
        return $this->belongsToMany(Group::class);
    }
}
