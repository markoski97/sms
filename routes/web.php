<?php

use Illuminate\Support\Facades\Route;

Route::resource('/profile','ProfileController');
Route::resource('/group','GroupController',['only' => ['index', 'create','store']]);

Route::get('group/addUser','GroupController@AddUser')->name('addUser');
Route::post('group/addUser','GroupController@StoreAddUser')->name('StoreAddUser');

Route::get('/sendsms','SmsController@index');
