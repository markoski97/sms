<?php

use Illuminate\Database\Seeder;

class UserTypesSeeder extends Seeder
{

    public function run()
    {
        DB::table('user_types')->insert([
            'name'=>'Individual'
        ]);

        DB::table('user_types')->insert([
            'name'=>'Legal entities'
        ]);
    }
}
