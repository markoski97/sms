@extends('layout')

@section('content')
<div class="container">
    <div class="jumbotron">
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

                    <div class="card-header">
                        Send SMS message
                    </div>
                    <div class="card-body">
                        <form method="POST" action="/custom">
                            @csrf
                            <div class="form-group">
                                <label>Select users to profiles</label>
                                <select name="profiles[]" multiple class="form-control">
                                    @foreach ($alls as $all)
                                        <option>{{$all->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Notification Message</label>
                                <textarea name="body" class="form-control"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Send Notification</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection


