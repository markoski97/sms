@extends('layout')

@section('content')
    <div class="container">
        <div class="jumbotron">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="card">
                <div class="card-header">
                    Add Profiles to Group
                </div>
                <div class="card-body">
                    <form method="POST" action="{{route('StoreAddUser')}}">
                        @csrf

                        <div class="form-group">
                            <label>Group</label>
                            <select id="group" class="form-control" name="group">
                                <option disabled selected>Chose one</option>
                                @foreach($groups as $group)
                                    <option value="{{$group->id}}">{{$group->name}}</option>
                                @endforeach
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Select users to notify</label>
                            <select name="profiles[]" multiple class="form-control">
                                @foreach ($profiles as $profile)
                                    <option>{{$profile->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <button type="submit" class="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
