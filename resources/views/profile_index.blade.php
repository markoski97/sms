@extends('layout')

@section('content')
    <div class="container">
        <div class="jumbotron">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <div class="card-header">
                    <a href="{{route('profile.create')}}" class="btn btn-info">To add profile Click here</a>

                </div>
            <table class="table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Type</th>
                    <th>Name and Surname</th>
                    <th>Email</th>
                    <th>Phone Number</th>
                    <th>Tax number</th>
                    <th>Created At:</th>
                    <th>Updated At:</th>
                    <th>Actions:</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Type</th>
                    <th>Name and Surname</th>
                    <th>Email</th>
                    <th>Phone Number</th>
                    <th>Tax number</th>
                    <th>Created At:</th>
                    <th>Updated At:</th>
                    <th>Actions:</th>
                </tr>
                </tfoot>
                <tbody>
                @foreach($profiles as $key=>$profile)
                    <tr>
                        <td>{{$key + 1 }}</td>
                        <td>{{$profile->type->name }}</td>
                        <td>{{$profile->name }}</td>
                        <td>{{$profile->email}}</td>
                        <td>{{$profile->phone_number}}</td>
                        <td>
                            @if($profile->tax_number==null)
                                No Tax Number this is an Individual
                                @else
                                {{$profile->tax_number}}
                            @endif
                        </td>
                        <td>{{$profile->created_at }}</td>
                        <td>{{$profile->updated_at }}</td>
                        <td class="text-center">
                            <a href="{{route('profile.edit',$profile->id)}}">
                                <button class="btn btn-success">  EDIT</button>

                            </a>

                                <form
                                    id="delete-form-{{$profile->id}}"
                                    action="{{route('profile.destroy',$profile->id)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                </form>
                                <button type="button" class="btn btn-danger"
                                        onclick="if(confirm('Are you sure'))
                                            {
                                            event.preventDefault();
                                            document.getElementById('delete-form-{{$profile->id}}').submit();
                                            }
                                            ">
                                    Delete
                                </button>




                        </td>

                    </tr>
                @endforeach


                </tbody>
            </table>
        </div>
    </div>
@endsection
