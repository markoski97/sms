@extends('layout')

@section('content')
    <div class="container">
        <div class="jumbotron">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            Add Profile
                        </div>
                        <div class="card-body">

                            <form method="POST" action="{{route('profile.store')}}">
                                @csrf
                                <div class="form-group">
                                    <label>Profile Type</label>
                                    <select id="test" class="form-control" name="type" onchange="showDiv('hidden_div', this)">
                                        <option disabled selected>Chose one</option>
                                        @foreach($types as $type)
                                            <option value="{{$type->id}}">{{$type->name}}</option>
                                        @endforeach
                                    </select>

                                </div>
                                <div class="form-group">
                                    <label>Enter Name and Surname</label>
                                    <input type="text" class="form-control" name="name" placeholder="Enter Name">
                                </div>
                                <div class="form-group">
                                    <label>Enter Email</label>
                                    <input type="tel" class="form-control" name="email" placeholder="Enter Email">
                                </div>
                                <div class="form-group">
                                    <label>Enter Phone Number</label>
                                    <input type="tel" class="form-control" name="phone_number"
                                           placeholder="Enter Phone Number">
                                </div>

                                <div class="form-group" id="hidden_div">
                                    <label>Enter Tax number</label>
                                    <input type="number" class="form-control" name="tax_number"
                                           placeholder="Enter Tax-Number">
                                </div>

                                <button type="submit" class="btn btn-primary">Register</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
