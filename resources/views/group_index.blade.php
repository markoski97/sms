@extends('layout')

@section('content')
    <div class="container">
        <div class="jumbotron">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <div class="card-header">
                    <a href="{{route('group.create')}}" class="btn btn-info">To add group Click here</a>

                </div>
            <table class="table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Created At:</th>
                    <th>Updated At:</th>

                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Created At:</th>
                    <th>Updated At:</th>
                </tr>
                </tfoot>
                <tbody>
                @foreach($groups as $key=>$group)
                    <tr>
                        <td>{{$key + 1 }}</td>
                        <td>{{$group->name }}</td>
                        <td>{{$group->created_at }}</td>
                        <td>{{$group->updated_at }}</td>
                    </tr>
                @endforeach


                </tbody>
            </table>
        </div>
    </div>
@endsection
