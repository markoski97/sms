@extends('layout')

@section('content')
    <div class="container">
        <div class="jumbotron">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            Add Group
                        </div>
                        <div class="card-body">

                            <form method="POST" action="{{route('group.store')}}">
                                @csrf
                                <div class="form-group">
                                    <label>Enter Name of the Group</label>
                                    <input type="text" class="form-control" name="name" placeholder="Enter Name of the Group">
                                </div>

                                <button type="submit" class="btn btn-primary">Register</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
